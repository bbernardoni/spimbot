# syscall constants
PRINT_STRING = 4
PRINT_CHAR   = 11
PRINT_INT    = 1

# debug constants
PRINT_INT_ADDR   = 0xffff0080
PRINT_FLOAT_ADDR = 0xffff0084
PRINT_HEX_ADDR   = 0xffff0088

# spimbot constants
VELOCITY       = 0xffff0010
ANGLE          = 0xffff0014
ANGLE_CONTROL  = 0xffff0018
BOT_X          = 0xffff0020
BOT_Y          = 0xffff0024
OTHER_BOT_X    = 0xffff00a0
OTHER_BOT_Y    = 0xffff00a4
TIMER          = 0xffff001c
SCORES_REQUEST = 0xffff1018

TILE_SCAN       = 0xffff0024
SEED_TILE       = 0xffff0054
WATER_TILE      = 0xffff002c
MAX_GROWTH_TILE = 0xffff0030
HARVEST_TILE    = 0xffff0020
BURN_TILE       = 0xffff0058
GET_FIRE_LOC    = 0xffff0028
PUT_OUT_FIRE    = 0xffff0040

GET_NUM_WATER_DROPS   = 0xffff0044
GET_NUM_SEEDS         = 0xffff0048
GET_NUM_FIRE_STARTERS = 0xffff004c
SET_RESOURCE_TYPE     = 0xffff00dc
REQUEST_PUZZLE        = 0xffff00d0
SUBMIT_SOLUTION       = 0xffff00d4

# interrupt constants
BONK_MASK               = 0x1000
BONK_ACK                = 0xffff0060
TIMER_MASK              = 0x8000
TIMER_ACK               = 0xffff006c
ON_FIRE_MASK            = 0x400
ON_FIRE_ACK             = 0xffff0050
MAX_GROWTH_ACK          = 0xffff005c
MAX_GROWTH_INT_MASK     = 0x2000
REQUEST_PUZZLE_ACK      = 0xffff00d8
REQUEST_PUZZLE_INT_MASK = 0x800

.data
puzzle: .space 16384 # 4096*4
soln:	.space 328
solnEnd:
domLUT: # packed as NNTTTT	
	.space 35 # op '+' 2*16+3=35
	.byte 3,5,15,10,12 # 3-7
	.space 12 # 8+4=12
	.byte 3,7,15,15,15,15,14,12 # 4-11
	.space 10 # 4+6=10
	.byte 3,7,15,15,15,15,15,14,12,0 # 6-14,15
cages:	.space 64
toDom:	.byte 0,1,2,4,8,16,32,64
subTar:	.byte 0,0,0,1,0,2,1,0,0,3,2,0,1,0,0,0
addTar:	.byte 0,1,2,3,3,4,5,6,4,5,6,7,7,8,9,10
shftAmt:.byte 28,24,20,16,12,8,4,0,28,24,20,16,12,8,4,0
nextPuzzle:	.word puzzle
curPuzzle:	.word puzzle
# 0 stopped
# 1 currently moving
# 2 planted need water
# 3 time to start moving
CURR_ACTION: .word 1
MOVE_LOCA: .word 0
LOOKUP_LOC_LEN = 48
LOOKUP_X_LOC: .byte 1,0,2,3,4,5,6,7,8,9,8,9,7,6,5,4 #16
              .byte 3,2,2,3,4,5,6,7,8,9,8,9,8,9,7,6 #32
              .byte 7,6,5,4,4,5,3,2,3,2,1,0,1,0,1,0 #48
LOOKUP_Y_LOC: .byte 1,0,0,1,0,1,0,1,0,1,2,3,3,2,3,2 #16
              .byte 3,2,4,5,4,5,4,5,4,5,6,7,8,9,9,8 #32
              .byte 7,6,7,6,8,9,9,8,7,6,7,6,5,4,3,2 #48
#LOOKUP_X_LOC: .byte 1,2,3,4,5,6,7,8,9,8,7,6,7,8,9,8,7,6,5,4,3,2,1,0,1,2,3,2,1,0
#LOOKUP_Y_LOC: .byte 0,1,2,3,2,1,0,1,2,3,4,5,6,7,8,9,8,7,6,7,8,9,8,7,6,5,4,3,2,1
TILES: .space 1600
SOLVED_FLAG: .word 0

 
.text
main:
    #initialization steps
	# enable interrupts
    li      $t0, REQUEST_PUZZLE_INT_MASK
    or      $t0, $t0, TIMER_MASK
    or      $t0, $t0, 1             # global interrupt enable
    mtc0    $t0, $12                # set interrupt mask (Status register)

    #set up the initial grabs of water
	li	$t0, 0
	sw	$t0, SET_RESOURCE_TYPE
	la	$t1, puzzle
	sw	$t1, REQUEST_PUZZLE
	add	$t1, 4096
	sw	$t1, REQUEST_PUZZLE
	add	$t1, 4096
	sw	$t1, REQUEST_PUZZLE
	add	$t1, 4096
	sw	$t1, REQUEST_PUZZLE

    #now init  
    jal move_init

#overall loop process
loop_process:
    #select the action to perform
    jal solvePuzzle
    j loop_process


	
select_action_puzzle:
    #check if there are no fire starters
    sub $sp, $sp, 4
    sw $ra, 0($sp)    
    lw $t1, GET_NUM_FIRE_STARTERS   
    bne $t1, $zero, not_fire_starter
    li $a0, 2 
    j done_select_action_puzzle

not_fire_starter:
    #check if there is    
    lw $t1, GET_NUM_SEEDS
    bgt $t1, 9, not_seeds 
    li $a0, 1
    j done_select_action_puzzle

not_seeds:
    #request water
    li $a0, 0
    j done_select_action_puzzle

done_select_action_puzzle:
	sw	$a0, SET_RESOURCE_TYPE
	lw	$t0, curPuzzle
	sw	$t0, REQUEST_PUZZLE
	add	$t0, $t0, 4096
	and	$t0, $t0, 0x10013FFF
	sw	$t0, curPuzzle
	sw	$zero, SOLVED_FLAG
    lw $ra, 0($sp) 
    add $sp, $sp, 4
    jr $ra   





# solve
solvePuzzle:
	sub	$sp, $sp, 24
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)
	sw	$s1, 8($sp)
	sw	$s2, 12($sp)
	sw	$s3, 16($sp)
	sw	$s4, 20($sp)
	la	$t1, nextPuzzle
	lw	$t0, curPuzzle
waitPuzzle:
	lw	$t2, 0($t1)
	beq	$t2, $t0, waitPuzzle
        la      $s0, soln
	lw	$s1, curPuzzle
	li	$s2, 0
	li	$s3, 0
	lw	$t0, 4($s1)
	add	$t4, $t0, 64
	li	$t5, 0
	li	$t6, 8
	la	$s4, cages
	# get DomLUT Byte
getDomLUTByte:
	lw	$t1, 4($t0)
	lw	$t2, 0($t1)
	bne	$t2, $zero, lowNoNop
	lw	$t2, 4($t1)
	lb	$t3, toDom($t2)
	sll	$s2, $s2, 4
	or	$s2, $s2, $t3
	j	lowOpCont
lowNoNop:
	lw	$t3, 12($t1)
	lw	$t3, 0($t3)
	bne	$t3, $t5, lowCageSkip
	sw	$t1, 0($s4)
	add	$s4, $s4, 4
lowCageSkip:
	beq	$t2, '+', lowOpAdd
	sll	$s2, $s2, 4
	or	$s2, $s2, 15
	j	lowOpCont
lowOpAdd:
	lw	$t2, 8($t1)
	sll	$t2, $t2, 4
	lw	$t3, 4($t1)
	or	$t2, $t2, $t3
	lb	$t3, domLUT($t2)
	sll	$s2, $s2, 4
	or	$s2, $s2, $t3
lowOpCont:
	add	$t0, $t0, 8
	add	$t5, $t5, 1
	# part2
	lw	$t1, 4($t4)
	lw	$t2, 0($t1)
	bne	$t2, $zero, highNoNop
	lw	$t2, 4($t1)
	lb	$t3, toDom($t2)
	sll	$s3, $s3, 4
	or	$s3, $s3, $t3
	j	highOpCont
highNoNop:
	lw	$t3, 12($t1)
	lw	$t3, 0($t3)
	bne	$t3, $t6, highCageSkip
	sw	$t1, 0($s4)
	add	$s4, $s4, 4
highCageSkip:
	beq	$t2, '+', highOpAdd
	sll	$s3, $s3, 4
	or	$s3, $s3, 15
	j	highOpCont
highOpAdd:
	lw	$t2, 8($t1)
	sll	$t2, $t2, 4
	lw	$t3, 4($t1)
	or	$t2, $t2, $t3
	lb	$t3, domLUT($t2)
	sll	$s3, $s3, 4
	or	$s3, $s3, $t3
highOpCont:
	add	$t4, $t4, 8
	add	$t6, $t6, 1
	bne	$t5, 8, getDomLUTByte
#reduce all
	li	$a0, 28
	li	$a1, 0x11110000
	li	$a2, 0x10001000
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	li	$a1, 0x1111
	sll	$a2, $a2, 12
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
	sub	$a0, $a0, 4
	srl	$a2, $a2, 4
	jal	reduceS2
	jal	reduceS3
#recurse
	li	$a0, 32
	jal	recurseS2
# submit solution
submitSoln:
	#convert soln
	li	$t0, 16
	sw	$t0, 0($s0)
	srl	$t0, $s2, 28
	srl	$t1, $s3, 28
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 4($s0)
	sw	$t1, 36($s0)
	srl	$t0, $s2, 24
	srl	$t1, $s3, 24
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 8($s0)
	sw	$t1, 40($s0)
	srl	$t0, $s2, 20
	srl	$t1, $s3, 20
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 12($s0)
	sw	$t1, 44($s0)
	srl	$t0, $s2, 16
	srl	$t1, $s3, 16
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 16($s0)
	sw	$t1, 48($s0)
	srl	$t0, $s2, 12
	srl	$t1, $s3, 12
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 20($s0)
	sw	$t1, 52($s0)
	srl	$t0, $s2, 8
	srl	$t1, $s3, 8
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 24($s0)
	sw	$t1, 56($s0)
	srl	$t0, $s2, 4
	srl	$t1, $s3, 4
	and	$t0, $t0, 15
	and	$t1, $t1, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 28($s0)
	sw	$t1, 60($s0)
	and	$t0, $s2, 15
	and	$t1, $s3, 15
	lb	$t0, addTar($t0)
	lb	$t1, addTar($t1)
	sw	$t0, 32($s0)
	sw	$t1, 64($s0)
        sw	$s0, SUBMIT_SOLUTION
	li	$t0, 1
	sw	$t0, SOLVED_FLAG
	jal	select_action_puzzle
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	add	$sp, $sp, 24
	jr	$ra

reduceS2:
	srl	$t0, $s2, $a0
	and	$t0, $t0, 15
	li	$t1, 0x116	# 0000 0001 0001 0110
	srl	$t1, $t1, $t0
	and	$t1, $t1, 1
	beq	$t1, $zero, s2MultiVal
reduceFS2:
	mul	$t1, $t0, $a1
	not	$t1, $t1
	and	$s2, $s2, $t1
	mul	$t1, $t0, $a2
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	sll	$t0, $t0, $a0
	or	$s2, $s2, $t0
s2MultiVal:
	jr	$ra

reduceS3:
	srl	$t0, $s3, $a0
	and	$t0, $t0, 15
	li	$t1, 0x116	# 0000 0001 0001 0110
	srl	$t1, $t1, $t0
	and	$t1, $t1, 1
	beq	$t1, $zero, s3MultiVal
reduceFS3:
	mul	$t1, $t0, $a1
	not	$t1, $t1
	and	$s3, $s3, $t1
	mul	$t1, $t0, $a2
	not	$t1, $t1
	and	$s3, $s3, $t1
	and	$s3, $s3, $t1
	sll	$t0, $t0, $a0
	or	$s3, $s3, $t0
s3MultiVal:
	jr	$ra

#for each 1 in dom:
#	set cell to val
#	reduce
#	check zero
#	recurse
recurseS2:
	sub	$a0, $a0, 4
	beq	$a0, -4, initRecurseS3
	sub	$sp, $sp, 12
	sw	$ra, 0($sp)
	sw	$s2, 4($sp)
	sw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
	li	$t2, 1
	sll	$t2, $t2, $a0
	and	$t3, $s2, $t2
	beq	$t3, $zero, valSkip1S2
	and	$t0, $a0, 0x10
	li	$t1, 0x1111
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x10001
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s2, $s2, $t4
	or	$s2, $s2, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS2
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip1S2:
	li	$t2, 2
	sll	$t2, $t2, $a0
	and	$t3, $s2, $t2
	beq	$t3, $zero, valSkip2S2
	and	$t0, $a0, 0x10
	li	$t1, 0x2222
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x20002
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s2, $s2, $t4
	or	$s2, $s2, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS2
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip2S2:
	li	$t2, 4
	sll	$t2, $t2, $a0
	and	$t3, $s2, $t2
	beq	$t3, $zero, valSkip3S2
	and	$t0, $a0, 0x10
	li	$t1, 0x4444
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x40004
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s2, $s2, $t4
	or	$s2, $s2, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS2
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip3S2:
	li	$t2, 8
	sll	$t2, $t2, $a0
	and	$t3, $s2, $t2
	beq	$t3, $zero, valSkip4S2
	and	$t0, $a0, 0x10
	li	$t1, 0x8888
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x80008
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s2, $s2, $t4
	or	$s2, $s2, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS2
valSkip4S2:
	lw	$ra, 0($sp)
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	add	$sp, $sp, 12
	add	$a0, $a0, 4
	jr	$ra

initRecurseS3:
	li	$a0, 32
recurseS3:
	sub	$a0, $a0, 4
	beq	$a0, -4, foundSoln
	sub	$sp, $sp, 12
	sw	$ra, 0($sp)
	sw	$s2, 4($sp)
	sw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
	li	$t2, 1
	sll	$t2, $t2, $a0
	and	$t3, $s3, $t2
	beq	$t3, $zero, valSkip1S3
	and	$t0, $a0, 0x10
	li	$t1, 0x1111
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s3, $s3, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x10001
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s3, $s3, $t4
	or	$s3, $s3, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS3
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip1S3:
	li	$t2, 2
	sll	$t2, $t2, $a0
	and	$t3, $s3, $t2
	beq	$t3, $zero, valSkip2S3
	and	$t0, $a0, 0x10
	li	$t1, 0x2222
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s3, $s3, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x20002
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s3, $s3, $t4
	or	$s3, $s3, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS3
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip2S3:
	li	$t2, 4
	sll	$t2, $t2, $a0
	and	$t3, $s3, $t2
	beq	$t3, $zero, valSkip3S3
	and	$t0, $a0, 0x10
	li	$t1, 0x4444
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s3, $s3, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x40004
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s3, $s3, $t4
	or	$s3, $s3, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS3
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	li	$t4, 15
	sll	$t4, $t4, $a0
	not	$t4, $t4
valSkip3S3:
	li	$t2, 8
	sll	$t2, $t2, $a0
	and	$t3, $s3, $t2
	beq	$t3, $zero, valSkip4S3
	and	$t0, $a0, 0x10
	li	$t1, 0x8888
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s3, $s3, $t1
	and	$t0, $a0, 0xf
	li	$t1, 0x80008
	sll	$t1, $t1, $t0
	not	$t1, $t1
	and	$s2, $s2, $t1
	and	$s3, $s3, $t1
	and	$s3, $s3, $t4
	or	$s3, $s3, $t2
	#check - recurse
	jal	checkInvaild
	jal	recurseS3
valSkip4S3:
	lw	$ra, 0($sp)
	lw	$s2, 4($sp)
	lw	$s3, 8($sp)
	add	$sp, $sp, 12
	add	$a0, $a0, 4
	bne	$a0, 32, normalRecurse
	li	$a0, 0
normalRecurse:
	jr	$ra

checkInvaild:
	# check for a 0
	and	$t0, $s2, 0xf
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf0
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf00
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf0000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf00000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf000000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s2, 0xf0000000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf0
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf00
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf0000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf00000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf000000
	beq	$t0, $zero, skipRecurse
	and	$t0, $s3, 0xf0000000
	beq	$t0, $zero, skipRecurse
	# check cages
	la	$t0, cages
checkCagesLoop:
	lw	$t1, 0($t0)
	lw	$t2, 0($t1)
	lw	$t3, 4($t1)
	lw	$t4, 12($t1)
	beq	$t2, '+', checkAdd
	lw	$t5, 0($t4)
	lb	$t1, shftAmt($t5)
	bge	$t5, 8, sub1S3
	srl	$t1, $s2, $t1
	j	sub1done
sub1S3:
	srl	$t1, $s3, $t1
sub1done:
	and	$t1, $t1, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t1
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	lw	$t5, 4($t4)
	lb	$t2, shftAmt($t5)
	bge	$t5, 8, sub2S3
	srl	$t2, $s2, $t2
	j	sub2done
sub2S3:
	srl	$t2, $s3, $t2
sub2done:
	and	$t2, $t2, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t2
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	or	$t1, $t1, $t2
	lb	$t1, subTar($t1)
	beq	$t1, $t3, checkCageDone
	j	skipRecurse
checkAdd:
	lw	$t6, 8($t1)
	lw	$t5, 0($t4)
	lb	$t1, shftAmt($t5)
	bge	$t5, 8, add1S3
	srl	$t1, $s2, $t1
	j	add1done
add1S3:
	srl	$t1, $s3, $t1
add1done:
	and	$t1, $t1, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t1
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	lw	$t5, 4($t4)
	lb	$t2, shftAmt($t5)
	bge	$t5, 8, add2S3
	srl	$t2, $s2, $t2
	j	add2done
add2S3:
	srl	$t2, $s3, $t2
add2done:
	and	$t2, $t2, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t2
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	lb	$t1, addTar($t1)
	lb	$t2, addTar($t2)
	add	$t1, $t1, $t2
	beq	$t6, 2, addFetchDone
	lw	$t5, 8($t4)
	lb	$t2, shftAmt($t5)
	bge	$t5, 8, add3S3
	srl	$t2, $s2, $t2
	j	add3done
add3S3:
	srl	$t2, $s3, $t2
add3done:
	and	$t2, $t2, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t2
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	lb	$t2, addTar($t2)
	add	$t1, $t1, $t2
	beq	$t6, 3, addFetchDone
	lw	$t5, 12($t4)
	lb	$t2, shftAmt($t5)
	bge	$t5, 8, add4S3
	srl	$t2, $s2, $t2
	j	add4done
add4S3:
	srl	$t2, $s3, $t2
add4done:
	and	$t2, $t2, 15
	li	$t5, 0x116
	srl	$t5, $t5, $t2
	and	$t5, $t5, 1
	beq	$t5, $zero, checkCageDone
	lb	$t2, addTar($t2)
	add	$t1, $t1, $t2
addFetchDone:
	bne	$t1, $t3, skipRecurse
checkCageDone:
	add	$t0, $t0, 4
	bne	$t0, $s4, checkCagesLoop
	jr	$ra
skipRecurse:
	add	$ra, $ra, 4
	jr	$ra

foundSoln:
	add	$sp, $sp, 192
	j	submitSoln


#MOVING STUFF
#______________________________
.data
three:	.float	3.0
five:	.float	5.0
PI:	.float	3.141592
F180:	.float  180.0
VELO_CYCLE: .word 1000

.text

# -----------------------------------------------------------------------
# move_init  
# moves to initial position
# returns the time before running to the set
# -----------------------------------------------------------------------

move_init:
	
	sub $sp, $sp, 4
	sw $ra, 0($sp)
	li $t0, 300		#placeholder for furthest distance
	li $t8, 2	
	li $t9, 1		#placeholders for default x and y coords
	li $t1, 0		#iterator

	add $t2, $zero, BOT_X
	add $t3, $zero, BOT_Y
	lw $t2, 0($t2)
	lw $t3, 0($t3)		#stores x and y value into t2 and t3
	sub $t2, $t2, 15
	div $t2, $t2, 30	#gets current x-coord
	sub $t3, $t3, 15
	div $t3, $t3, 30	#gets current y-coord
	
	la $t4, LOOKUP_X_LOC
	la $t5, LOOKUP_Y_LOC		#stores x/y arrays into t4 and t5
loop_init_loc:
	bge $t1, LOOKUP_LOC_LEN, loop_init_loc_done
	#mul $t6, $t1, 4	#iterator * 4
	add $t6, $t4, $t1	#t6 location of Xarray[i]
	lb $t6, 0($t6)		#x-coord of Xarray[i]

	#mul $t7, $t1, 4		#iterator * 4
	add $t7, $t5, $t1	# t7 location of Yarray[i]
	lb $t7, 0($t7)		#y-coord of Yarray[i]

	sub $a0, $t2, $t6
	sub $a1, $t3, $t7
	la $a2, euclidean_dist
	jalr $a2

	add $t1, $t1, 1		#iterator++
	bgt	$v0, $t0, further
	move $t0, $v0
	move $t8, $t6
	move $t9, $t7
	sub $t1, $t1, 1
	sw $t1, MOVE_LOCA($zero) 	#move MOVE_LOCA, $t1
	add $t1, $t1, 1
	
further:
	j loop_init_loc
loop_init_loc_done:


	move $a0, $t8
	move $a1, $t9
	la $a2, move_tile_center
	jalr $a2
	lw $ra 0($sp)
	add $sp, $sp, 4
    jr $ra


# -----------------------------------------------------------------------

.kdata                          # interrupt handler data (separated just for readability)
chunkIH:        .space 8        # space for two registers
chunkTIMER:        .space 64

.ktext 0x80000180
interrupt_handler:
.set noat
        move    $k1, $at                # Save $at                               
.set at
        la      $k0, chunkIH
        sw      $a0, 0($k0)             # Get some free registers                  
        sw      $a1, 4($k0)             # by storing them to a global variable     

        mfc0    $k0, $13                # Get Cause register                       
        srl     $a0, $k0, 2                
        and     $a0, $a0, 0xf           # ExcCode field                            
        bne     $a0, 0, done         

interrupt_dispatch:                     # Interrupt:                             
        mfc0    $k0, $13                # Get Cause register, again                 
        beq     $k0, 0, done            # handled all outstanding interrupts     
        
        and     $a0, $k0, BONK_MASK     # is there a bonk interrupt?                
        bne     $a0, 0, bonk_interrupt   
        and     $a0, $k0, TIMER_MASK    # is there a timer interrupt?
        bne     $a0, 0, timer_interrupt
        and     $a0, $k0, ON_FIRE_MASK  # is there a fire interrupt?
        bne     $a0, 0, fire_interrupt
        and     $a0, $k0, MAX_GROWTH_INT_MASK  # is there a max growth interrupt?
        bne     $a0, 0, max_growth_interrupt
        and     $a0, $k0, REQUEST_PUZZLE_INT_MASK  # is there a request puzzle interrupt?
        bne     $a0, 0, request_puzzle_interrupt
        
        j       done

bonk_interrupt:
        sw      $a1, BONK_ACK           # acknowledge interrupt
        sw      $zero, VELOCITY         # ???
        j       interrupt_dispatch      # see if other interrupts are waiting

timer_interrupt: 
        sw      $zero, VELOCITY
        sw      $a1, TIMER_ACK          # acknowledge interrupt
        sw      $zero, CURR_ACTION 

        la      $a1, chunkTIMER 
        sw      $t0, 0($a1)
        sw      $t1, 4($a1)
        sw      $t2, 8($a1)

        sw      $t0, 12($a1) 
        sw      $v0, 16($a1)
        sw      $v1, 20($a1)
        sw      $ra, 24($a1) 

        sw      $s0, 28($a1)
        sw      $s1, 32($a1) 

        #now call the relevant function calls

action_timer:
	jal select_action_tile
	move $a0, $v0
	#now do actions based on what should be done 
	beq $a0, 4, end_resource
	jal perform_action
        j action_timer

end_resource:

	# move
	lw	$t0, MOVE_LOCA
	add	$t0, $t0, 1
	rem	$t0, $t0, LOOKUP_LOC_LEN
	sw	$t0, MOVE_LOCA
	lb	$a0, LOOKUP_X_LOC($t0)
	lb	$a1, LOOKUP_Y_LOC($t0)
	jal	move_tile_corner

        la      $a1, chunkTIMER 
        lw      $t0, 0($a1)
        lw      $t1, 4($a1)
        lw      $t2, 8($a1)

        lw      $t0, 12($a1) 
        lw      $v0, 16($a1)
        lw      $v1, 20($a1)
        lw      $ra, 24($a1) 
        lw      $s0, 28($a1)
        lw      $s1, 32($a1) 

        j       interrupt_dispatch      # see if other interrupts are waiting

fire_interrupt:
        sw      $a1, ON_FIRE_ACK        # acknowledge interrupt
        j       interrupt_dispatch      # see if other interrupts are waiting

max_growth_interrupt:
        sw      $a1, MAX_GROWTH_ACK     # acknowledge interrupt
        j       interrupt_dispatch      # see if other interrupts are waiting

request_puzzle_interrupt:
        sw      $a1, REQUEST_PUZZLE_ACK # acknowledge interrupt
	lw	$a1, nextPuzzle
	add	$a1, $a1, 4096
	and	$a1, $a1, 0x10013FFF
	sw	$a1, nextPuzzle
        j       interrupt_dispatch      # see if other interrupts are waiting

done:
        la      $k0, chunkIH
        lw      $a0, 0($k0)             # Restore saved registers
        lw      $a1, 4($k0)
.set noat
        move    $at, $k1                # Restore $at
.set at 
        eret

# -----------------------------------------------------------------------
# select_action 
# 
#
# returns a number based on the action that should be performed
#   0-2 request a source
#       0 for water
#       1 for seed
#       2 for fire 
#   3 for solve the puzzle
#   4 for move to next location 
#   5-8 do something at current location  
#       5 for plant seed
#       6 for water plant
#       7 for harvest plant
#       8 for set fire  
# 
# -----------------------------------------------------------------------
select_action_tile:
    #branching for various resource requests 
    sub $sp, $sp, 4
    sw $ra, 0($sp)    

not_requesting:
    #differnt jumps based on the moving flag 
    lw $t0, CURR_ACTION  
    #branch if it's not perfomring anything on the tile
    #or if it's moving
    #bne $t0, 1, not_solve 
    #li $v0, 3 
    #j done_select_action 

not_solve:
    #determine if it is a movement or not
    bne $t0, 3, not_move
    li $v0, 4
    j done_select_action

not_move:
#drop through to see if 
#what really needs to be done on the tile
    bne $t0, 2, not_water
    li $v0, 6
    j done_select_action

not_water:
    #call helper function to find the current state and owner
    jal find_tile
    lw $t0, 0($v0)
    lw $t1, 4($v0)
    bne $t0, $zero, not_empty_tile
    li $v0, 5
    j done_select_action

not_empty_tile:
    #determine if need to move or needs to solve
    bne $t1, $zero, not_my_tile_select
    lw $t0, 12($v0)
    #since we always water our plants, water is only zero when a tile is burning
    beq $t0, $zero, move_past_our_fire
    li $v0, 7
    j done_select_action

move_past_our_fire:
    li $v0, 4
    j done_select_action

not_my_tile_select:  
    #need to set it on fire
    li $v0, 8

done_select_action:
    lw $ra, 0($sp) 
    add $sp, $sp, 4
    jr $ra   

# -----------------------------------------------------------------------
# find_curr_state_and_owner
# #no arguments   
# returns v0 as pointer to TileInfo struct
# performs intended action 
# -----------------------------------------------------------------------
find_tile:
    la $t0, TILES   
    sw $t0, TILE_SCAN
    
    #now find the current tile location    
    #find the number that 5 needs to update to    
    lw $t1, BOT_X
    lw $t2, BOT_Y  

    #figure out which x-y tyles are being used
    div $t1, $t1, 30
    div $t2, $t2, 30 

    #now correctly offset the y corrdinate for the array 
    mul $t2, $t2, 10
    add $t1, $t1, $t2

    #now set the right memory location for the tile structure
    mul $t1, $t1, 16
    add $v0, $t0, $t1

    jr $ra

# -----------------------------------------------------------------------
# select_action 
# argument 0 is from the selection action
# returns nothing
# performs intended action 
# -----------------------------------------------------------------------

perform_action:
	sub	$sp, $sp, 4
	sw	$ra, 0($sp)
notRequest:
	bgt	$a0, 3, notSolve
	# solve puzzle
	#jal	solvePuzzle
	j	perform_action_done
notSolve:
	bgt	$a0, 4, notMove
	# move
	j	perform_action_done
notMove:
	bgt	$a0, 5, notPlant
	# plant
	sw	$zero, SEED_TILE
	li	$t1, 2
	sw	$t1, CURR_ACTION
	j	perform_action_done
notPlant:
	bgt	$a0, 6, notWater
	# water
	li	$t0, 3
	sw	$t0, WATER_TILE
	li	$t1, 3
	sw	$t1, CURR_ACTION
	j	perform_action_done
notWater:
	bgt	$a0, 7, notHarvest
	# harvest
	sw	$zero, HARVEST_TILE
	sw	$zero, CURR_ACTION
	j	perform_action_done
notHarvest:
	# set fire
	sw	$zero, BURN_TILE
	li	$t1, 3
	sw	$t1, CURR_ACTION
	j	perform_action_done
perform_action_done:
	lw	$ra, 0($sp)
	add	$sp, $sp, 4
	jr	$ra

# -----------------------------------------------------------------------
# move_bot - computes the arctangent of y / x
# $a0 - x
# $a1 - y
# returns number of cycles ancipated for the move
# 
# -----------------------------------------------------------------------

move_bot:
    sub $sp, $sp, 12
    sw $ra, 0($sp)
    sw $s0, 4($sp)
    sw $s1, 8($sp)
    
    #now set the speed of the bot to zero and 
    # determine the number X-Y to be held 
    sw $zero, VELOCITY
    lw $s0, BOT_X
    lw $s1, BOT_Y

    #Now figure out the delta X-Y necessary
    sub $s0, $a0, $s0
    sub $s1, $a1, $s1

    #figure out the angle information
    move $a0, $s0
    move $a1, $s1
    jal sb_arctan 

    li $t0, 1
    sw $v0, ANGLE
    sw $t0, ANGLE_CONTROL

    #start the bot to move
    li $t1, 10
    sw $t1, VELOCITY
    li $t1, 1
    sw $t1, CURR_ACTION
     

    #figure out the timer and the distance information 
    move $a0, $s0
    move $a1, $s1
    jal euclidean_dist
    lw $t0, TIMER
    lw $t1, VELO_CYCLE 
    mul $v0, $v0, $t1
    add $t0, $t0, $v0
    sw $t0, TIMER

    #restore everything
    lw $ra, 0($sp)
    lw $s0, 4($sp)
    lw $s1, 8($sp)
    add $sp, $sp, 12
    jr $ra


# -----------------------------------------------------------------------
# move_tile_center - moves to the center spot of a select X-Y tile
# $a0 - x
# $a1 - y
# returns the time before running to the set
# -----------------------------------------------------------------------
move_tile_center:
    sub $sp, $sp, 4
    sw $ra, 0($sp)
    mul $a0,$a0, 30 
    mul $a1,$a1, 30
    add $a0,$a0, 15
    add $a1,$a1, 15
    jal move_bot
    lw $ra, 0($sp)
    add $sp, $sp, 4
    jr $ra


# -----------------------------------------------------------------------
# move_tile_corner - moves to the closest spot of a select X-Y tile
# $a0 - x
# $a1 - y
# returns the time before running to the set
# -----------------------------------------------------------------------
move_tile_corner:
    sub $sp, $sp, 4
    sw $ra, 0($sp)
    lw $t0, BOT_X
    mul $a0,$a0, 30 
    add $a0,$a0, 5
    ble $t0,$a0, mtc_x_done
    add $a0,$a0, 19
    bge $t0,$a0, mtc_x_done
    move $a0, $t0
mtc_x_done:
    lw $t0, BOT_Y
    mul $a1,$a1, 30 
    add $a1,$a1, 5
    ble $t0,$a1, mtc_y_done
    add $a1,$a1, 19
    bge $t0,$a1, mtc_y_done
    move $a1, $t0
mtc_y_done:
    jal move_bot
    lw $ra, 0($sp)
    add $sp, $sp, 4
    jr $ra


# -----------------------------------------------------------------------
# sb_arctan - computes the arctangent of y / x
# $a0 - x
# $a1 - y
# returns the arctangent
# -----------------------------------------------------------------------

sb_arctan:
	li	$v0, 0		# angle = 0;

	abs	$t0, $a0	# get absolute values
	abs	$t1, $a1
	ble	$t1, $t0, no_TURN_90	  

	## if (abs(y) > abs(x)) { rotate 90 degrees }
	move	$t0, $a1	# int temp = y;
	neg	$a1, $a0	# y = -x;      
	move	$a0, $t0	# x = temp;    
	li	$v0, 90		# angle = 90;  

no_TURN_90:
	bgez	$a0, pos_x 	# skip if (x >= 0)

	## if (x < 0) 
	add	$v0, $v0, 180	# angle += 180;

pos_x:
	mtc1	$a0, $f0
	mtc1	$a1, $f1
	cvt.s.w $f0, $f0	# convert from ints to floats
	cvt.s.w $f1, $f1
	
	div.s	$f0, $f1, $f0	# float v = (float) y / (float) x;

	mul.s	$f1, $f0, $f0	# v^^2
	mul.s	$f2, $f1, $f0	# v^^3
	l.s	$f3, three	# load 5.0
	div.s 	$f3, $f2, $f3	# v^^3/3
	sub.s	$f6, $f0, $f3	# v - v^^3/3

	mul.s	$f4, $f1, $f2	# v^^5
	l.s	$f5, five	# load 3.0
	div.s 	$f5, $f4, $f5	# v^^5/5
	add.s	$f6, $f6, $f5	# value = v - v^^3/3 + v^^5/5

	l.s	$f8, PI		# load PI
	div.s	$f6, $f6, $f8	# value / PI
	l.s	$f7, F180	# load 180.0
	mul.s	$f6, $f6, $f7	# 180.0 * value / PI

	cvt.w.s $f6, $f6	# convert "delta" back to integer
	mfc1	$t0, $f6
	add	$v0, $v0, $t0	# angle += delta

	jr 	$ra
	


# -----------------------------------------------------------------------
# euclidean_dist - computes sqrt(x^2 + y^2)
# $a0 - x
# $a1 - y
# returns the distance
# -----------------------------------------------------------------------

euclidean_dist:
	mul	$a0, $a0, $a0	# x^2
	mul	$a1, $a1, $a1	# y^2
	add	$v0, $a0, $a1	# x^2 + y^2
	mtc1	$v0, $f0
	cvt.s.w	$f0, $f0	# float(x^2 + y^2)
	sqrt.s	$f0, $f0	# sqrt(x^2 + y^2)
	cvt.w.s	$f0, $f0	# int(sqrt(...))
	mfc1	$v0, $f0
	jr	$ra

