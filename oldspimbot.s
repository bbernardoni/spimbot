# syscall constants
PRINT_STRING = 4
PRINT_CHAR   = 11
PRINT_INT    = 1

# debug constants
PRINT_INT_ADDR   = 0xffff0080
PRINT_FLOAT_ADDR = 0xffff0084
PRINT_HEX_ADDR   = 0xffff0088

# spimbot constants
VELOCITY       = 0xffff0010
ANGLE          = 0xffff0014
ANGLE_CONTROL  = 0xffff0018
BOT_X          = 0xffff0020
BOT_Y          = 0xffff0024
OTHER_BOT_X    = 0xffff00a0
OTHER_BOT_Y    = 0xffff00a4
TIMER          = 0xffff001c
SCORES_REQUEST = 0xffff1018

TILE_SCAN       = 0xffff0024
SEED_TILE       = 0xffff0054
WATER_TILE      = 0xffff002c
MAX_GROWTH_TILE = 0xffff0030
HARVEST_TILE    = 0xffff0020
BURN_TILE       = 0xffff0058
GET_FIRE_LOC    = 0xffff0028
PUT_OUT_FIRE    = 0xffff0040

GET_NUM_WATER_DROPS   = 0xffff0044
GET_NUM_SEEDS         = 0xffff0048
GET_NUM_FIRE_STARTERS = 0xffff004c
SET_RESOURCE_TYPE     = 0xffff00dc
REQUEST_PUZZLE        = 0xffff00d0
SUBMIT_SOLUTION       = 0xffff00d4

# interrupt constants
BONK_MASK               = 0x1000
BONK_ACK                = 0xffff0060
TIMER_MASK              = 0x8000
TIMER_ACK               = 0xffff006c
ON_FIRE_MASK            = 0x400
ON_FIRE_ACK             = 0xffff0050
MAX_GROWTH_ACK          = 0xffff005c
MAX_GROWTH_INT_MASK     = 0x2000
REQUEST_PUZZLE_ACK      = 0xffff00d8
REQUEST_PUZZLE_INT_MASK = 0x800

.data
puzzle: .space 65536#4096
soln:	.space 328
solnEnd:
lock:	.word puzzle

.text
main:
	# enable interrupts
        li      $t0, REQUEST_PUZZLE_INT_MASK
        or      $t0, $t0, 1             # global interrupt enable
        mtc0    $t0, $12                # set interrupt mask (Status register)
	li      $t0, 1
        sw      $t0, SET_RESOURCE_TYPE
        la      $t1, puzzle
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
        add     $t1, 4096
        sw      $t1, REQUEST_PUZZLE
	la      $s1, puzzle
loopPuz:
	move	$a1, $s1
        la      $a0, soln	# zero out soln
	move	$t1, $a0
	la      $t2, solnEnd
clearSoln:
        sw      $0, 0($t1)
	add	$t1, $t1, 4
	bne	$t1, $t2, clearSoln
        la      $t1, lock
waitPuzzle:
        lw      $t0, 0($t1)
        beq     $t0, $s1, waitPuzzle
        jal     recursive_backtracking
        la      $t0, soln
        sw	$t0, SUBMIT_SOLUTION
	sw      $s1, REQUEST_PUZZLE
        add     $s1, $s1, 4096
        and     $s1, $s1, 0x1001FFFF
        or      $s1, $s1, 0x10010000
	j	loopPuz

recursive_backtracking:
        sub     $sp, $sp, 24
        sw      $ra, 0($sp)
        sw      $s0, 4($sp)
        sw      $s1, 8($sp)
        sw      $s2, 12($sp)
        sw      $a0, 16($sp)
        sw      $a1, 20($sp)
        jal     is_complete             # if(is_complete(solution, puzzle))
        beq     $v0, $zero, rBack_skp_ret
        li      $v0, 1
        j       rBack_ret               #   return 1;
rBack_skp_ret:
        lw      $a0, 16($sp)
        lw      $a1, 20($sp)
        jal     get_unassigned_position
        lw      $a0, 16($sp)
        lw      $a1, 20($sp)
        move    $s0, $v0                # int position = get_unassigned_position(solution, puzzle);
        li      $s1, 1                  # int val = 1;
rBack_for:
        lw      $t0, 0($a1)
        add     $t0, $t0, 1
        bge     $s1, $t0, rBack_for_end # for (int val = 1; val < puzzle->size + 1; val++)
        lw      $t0, 4($a1)     # puzzle->grid
        sll     $t1, $s0, 3
        add     $t0, $t0, $t1   # &puzzle->grid[position]
        lw      $t0, 0($t0)     # puzzle->grid[position].domain
        sub     $t1, $s1, 1
        li      $t2, 1
        sll     $t1, $t2, $t1
        and     $t0, $t0, $t1
        beq     $t0, $zero, rBack_skp_if # if (puzzle->grid[position].domain & (0x1 << (val - 1)))
        sll     $t0, $s0, 2
        add     $t0, $t0, $a0
        sw      $s1, 4($t0)             # solution->assignment[position] = val;
        lw      $t0, 0($a0)
        add     $t0, $t0, 1
        sw      $t0, 0($a0)             # solution->size += 1;
        move    $s2, $sp
        sub     $sp, $sp, 656   # sizeof(Puzzle) + sizeof(Cell[81])
        add     $t0, $sp, 8
        sw      $t0, 4($sp)             # puzzle_copy.grid = grid_copy;
        move    $a0, $a1
        move    $a1, $sp
        jal     clone                   # clone(puzzle, &puzzle_copy);
        lw      $a0, 16($s2)
        lw      $a1, 20($s2)
        add     $t0, $sp, 8             # &puzzle_copy.grid[0]
        sll     $t1, $s0, 3
        add     $t0, $t0, $t1           # &puzzle_copy.grid[position].domain
        sub     $t1, $s1, 1
        li      $t2, 1
        sll     $t1, $t2, $t1
        sw      $t1, 0($t0)             # puzzle_copy.grid[position].domain = 0x1 << (val - 1);
        move    $a0, $s0
        move    $a1, $sp
        jal     forward_checking        # if (forward_checking(position, &puzzle_copy))
        lw      $a0, 16($s2)
        lw      $a1, 20($s2)
        beq     $v0, $zero, rBack_skp_fc
        move    $a1, $sp
        jal     recursive_backtracking  # if (recursive_backtracking(solution, &puzzle_copy))
        lw      $a0, 16($s2)
        lw      $a1, 20($s2)
        beq     $v0, $zero, rBack_skp_rb
        add     $sp, $sp, 656
        li      $v0, 1                  # return 1;
        j       rBack_ret
rBack_skp_rb:
rBack_skp_fc:
        sll     $t0, $s0, 2
        add     $t0, $t0, $a0
        sw      $zero, 4($t0)           # solution->assignment[position] = 0;
        lw      $t0, 0($a0)
        sub     $t0, $t0, 1
        sw      $t0, 0($a0)             # solution->size -= 1;
        add     $sp, $sp, 656
rBack_skp_if:
        add     $s1, $s1, 1
        j       rBack_for
rBack_for_end:
        li      $v0, 0
rBack_ret:
        lw      $ra, 0($sp)
        lw      $s0, 4($sp)
        lw      $s1, 8($sp)
        lw      $s2, 12($sp)
        add     $sp, $sp, 24
        jr  $ra

is_complete:
        lw      $t0, 0($a1)
        mul     $t0, $t0, $t0
        lw      $t1, 0($a0)
        beq     $t0, $t1, ic_equal
        li      $v0, 0
        jr      $ra
ic_equal:
        li      $v0, 1
        jr      $ra

get_unassigned_position:
        move    $v0, $zero
        add     $t1, $a0, 4
gup_for:
        lw      $t0, 0($a1)
        mul     $t0, $t0, $t0
        bge     $v0, $t0, gup_for_end
        lw      $t2, 0($t1)
        beq     $t2, $zero, gup_for_end
        add     $t1, $t1, 4
        add     $v0, $v0, 1
        j gup_for
gup_for_end:
        jr      $ra

clone:
    lw  $t0, 0($a0)
    sw  $t0, 0($a1)
    mul $t0, $t0, $t0
    mul $t0, $t0, 2 # two words in one grid
    lw  $t1, 4($a0) # &puzzle(ori).grid
    lw  $t2, 4($a1) # &puzzle(clone).grid
    li  $t3, 0 # i = 0;
clone_for_loop:
    bge  $t3, $t0, clone_for_loop_end
    sll $t4, $t3, 2 # i * 4
    add $t5, $t1, $t4 # puzzle(ori).grid ith word
    lw   $t6, 0($t5)
    add $t5, $t2, $t4 # puzzle(clone).grid ith word
    sw   $t6, 0($t5)
    addi $t3, $t3, 1 # i++
    j    clone_for_loop
clone_for_loop_end:
    jr  $ra

forward_checking:
        lw      $t0, 0($a1)
        lw      $t3, 4($a1)
        sll     $t4, $a0, 3
        add     $t4, $t4, $t3
        li      $v0, 0
        ble     $t0, $zero, fc_cage
        move    $t1, $zero
fc_col_for:
        rem     $t2, $a0, $t0
        beq     $t1, $t2, fc_col_cont
        div     $t2, $a0, $t0
        mul     $t2, $t2, $t0
        add     $t2, $t2, $t1
        sll     $t2, $t2, 3
        add     $t2, $t2, $t3
        lw      $t6, 0($t4)
        not     $t6, $t6
        lw      $t5, 0($t2)
        and     $t5, $t5, $t6
        sw      $t5, 0($t2)
        bne     $t5, $zero, fc_col_cont
        jr      $ra
fc_col_cont:
        add     $t1, $t1, 1
        blt     $t1, $t0, fc_col_for
        move    $t1, $zero
fc_row_for:
        div     $t2, $a0, $t0
        beq     $t1, $t2, fc_row_cont
        mul     $t2, $t1, $t0
        rem     $t5, $a0, $t0
        add     $t2, $t2, $t5
        sll     $t2, $t2, 3
        add     $t2, $t2, $t3
        lw      $t6, 0($t4)
        not     $t6, $t6
        lw      $t5, 0($t2)
        and     $t5, $t5, $t6
        sw      $t5, 0($t2)
        bne     $t5, $zero, fc_row_cont
        jr      $ra
fc_row_cont:
        add     $t1, $t1, 1
        blt     $t1, $t0, fc_row_for
fc_cage:
        move    $t1, $zero
        lw      $t0, 4($t4)
fc_cage_for:
        lw      $t2, 8($t0)
        bge     $t1, $t2, fc_cage_end
        sll     $t2, $t1, 2
        lw      $a0, 12($t0)
        add     $a0, $a0, $t2
        lw      $a0, 0($a0)
        sub     $sp, $sp, 28
        sw      $ra, 0($sp)
        sw      $a1, 4($sp)
        sw      $a0, 8($sp)
        sw      $t0, 12($sp)
        sw      $t1, 16($sp)
        sw      $t3, 20($sp)
        sw      $t4, 24($sp)
        jal     get_domain_for_cell
        lw      $ra, 0($sp)
        lw      $a1, 4($sp)
        lw      $t2, 8($sp)
        lw      $t0, 12($sp)
        lw      $t1, 16($sp)
        lw      $t3, 20($sp)
        lw      $t4, 24($sp)
        add     $sp, $sp, 28
        sll     $t2, $t2, 3
        add     $t2, $t2, $t3
        lw      $t5, 0($t2)
        and     $t5, $t5, $v0
        sw      $t5, 0($t2)
        bne     $t5, $zero, fc_cage_cont
        li      $v0, 0
        jr      $ra
fc_cage_cont:
        add     $t1, $t1, 1
        j       fc_cage_for
fc_cage_end:
        li      $v0, 1
        jr      $ra

get_domain_for_cell:
    # save registers    
    sub $sp, $sp, 36
    sw $ra, 0($sp)
    sw $s0, 4($sp)
    sw $s1, 8($sp)
    sw $s2, 12($sp)
    sw $s3, 16($sp)
    sw $s4, 20($sp)
    sw $s5, 24($sp)
    sw $s6, 28($sp)
    sw $s7, 32($sp)
    li $t0, 0 # valid_domain
    lw $t1, 4($a1) # puzzle->grid (t1 free)
    sll $t2, $a0, 3 # position*8 (actual offset) (t2 free)
    add $t3, $t1, $t2 # &puzzle->grid[position]
    lw  $t4, 4($t3) # &puzzle->grid[position].cage
    lw  $t5, 0($t4) # puzzle->grid[posiition].cage->operation
    lw $t2, 4($t4) # puzzle->grid[position].cage->target
    move $s0, $t2   # remain_target = $s0  *!*!
    lw $s1, 8($t4) # remain_cell = $s1 = puzzle->grid[position].cage->num_cell
    lw $s2, 0($t3) # domain_union = $s2 = puzzle->grid[position].domain
    move $s3, $t4 # puzzle->grid[position].cage
    li $s4, 0   # i = 0
    move $s5, $t1 # $s5 = puzzle->grid
    move $s6, $a0 # $s6 = position
    # move $s7, $s2 # $s7 = puzzle->grid[position].domain
    bne $t5, 0, gdfc_check_else_if
    li $t1, 1
    sub $t2, $t2, $t1 # (puzzle->grid[position].cage->target-1)
    sll $v0, $t1, $t2 # valid_domain = 0x1 << (prev line comment)
    j gdfc_end # somewhere!!!!!!!!
gdfc_check_else_if:
    bne $t5, '+', gdfc_check_else
gdfc_else_if_loop:
    lw $t5, 8($s3) # puzzle->grid[position].cage->num_cell
    bge $s4, $t5, gdfc_for_end # branch if i >= puzzle->grid[position].cage->num_cell
    sll $t1, $s4, 2 # i*4
    lw $t6, 12($s3) # puzzle->grid[position].cage->positions
    add $t1, $t6, $t1 # &puzzle->grid[position].cage->positions[i]
    lw $t1, 0($t1) # pos = puzzle->grid[position].cage->positions[i]
    add $s4, $s4, 1 # i++
    sll $t2, $t1, 3 # pos * 8
    add $s7, $s5, $t2 # &puzzle->grid[pos]
    lw  $s7, 0($s7) # puzzle->grid[pos].domain
    beq $t1, $s6 gdfc_else_if_else # branch if pos == position
    move $a0, $s7 # $a0 = puzzle->grid[pos].domain
    jal is_single_value_domain
    bne $v0, 1 gdfc_else_if_else # branch if !is_single_value_domain()
    move $a0, $s7
    jal convert_highest_bit_to_int
    sub $s0, $s0, $v0 # remain_target -= convert_highest_bit_to_int
    addi $s1, $s1, -1 # remain_cell -= 1
    j gdfc_else_if_loop
gdfc_else_if_else:
    or $s2, $s2, $s7 # domain_union |= puzzle->grid[pos].domain
    j gdfc_else_if_loop
gdfc_for_end:
    move $a0, $s0
    move $a1, $s1
    move $a2, $s2
    ble $a0, $zero, gdfc_add_err
    jal get_domain_for_addition # $v0 = valid_domain = get_domain_for_addition()
    j gdfc_end
gdfc_add_err:
    li $v0, 0
    j gdfc_end
gdfc_check_else:
    lw $t3, 12($s3) # puzzle->grid[position].cage->positions
    lw $t0, 0($t3) # puzzle->grid[position].cage->positions[0]
    lw $t1, 4($t3) # puzzle->grid[position].cage->positions[1]
    xor $t0, $t0, $t1
    xor $t0, $t0, $s6 # other_pos = $t0 = $t0 ^ position
    lw $a0, 4($s3) # puzzle->grid[position].cage->target
    sll $t2, $s6, 3 # position * 8
    add $a1, $s5, $t2 # &puzzle->grid[position]
    lw  $a1, 0($a1) # puzzle->grid[position].domain
    # move $a1, $s7 
    sll $t1, $t0, 3 # other_pos*8 (actual offset)
    add $t3, $s5, $t1 # &puzzle->grid[other_pos]
    lw $a2, 0($t3)  # puzzle->grid[other_pos].domian
    jal get_domain_for_subtraction # $v0 = valid_domain = get_domain_for_subtraction()
    # j gdfc_end
gdfc_end:
# restore registers
    lw $ra, 0($sp)
    lw $s0, 4($sp)
    lw $s1, 8($sp)
    lw $s2, 12($sp)
    lw $s3, 16($sp)
    lw $s4, 20($sp)
    lw $s5, 24($sp)
    lw $s6, 28($sp)
    lw $s7, 32($sp)
    add $sp, $sp, 36    
    jr $ra

convert_highest_bit_to_int:
        move    $v0, $zero
chbtiFor:
        beq     $a0, $zero, chbtiRet
        addi    $v0, $v0, 1
        srl     $a0, $a0, 1
        j chbtiFor
chbtiRet:
        jr      $ra

get_domain_for_addition:
        sub     $sp, $sp, 20
        sw      $ra, 0($sp)
        sw      $a0, 4($sp)
        sw      $a1, 8($sp)
        sw      $a2, 12($sp)
        move    $a0, $a2
        jal     convert_highest_bit_to_int
        sw      $v0, 16($sp)
        lw      $a0, 12($sp)
        neg     $t0, $a0
        and     $a0, $a0, $t0
        jal     convert_highest_bit_to_int
        lw      $t2, 8($sp)     # t2=num-1
        sub     $t2, $t2, 1
        move    $t3, $v0        # t3=low_b
        lw      $t4, 16($sp)    # t4=up_b
        lw      $t5, 4($sp)     # t5=target
        lw      $v0, 12($sp)    # v0=domain
        mul     $t0, $t2, $t3
        sub     $t0, $t5, $t0
        bge     $t0, $t4, gdfaSp1
        li      $t1, 1
        sll     $t0, $t1, $t0
        sub     $t0, $t0, 1
        and     $v0, $v0, $t0
gdfaSp1:
        mul     $t0, $t2, $t4
        sub     $t0, $t5, $t0
        ble     $t0, $zero, gdfaSp2
        sub     $t0, $t0, 1
        srl     $t1, $v0, $t0
        sll     $v0, $t1, $t0
gdfaSp2:
        lw      $ra, 0($sp)
        add     $sp, $sp, 20
        jr      $ra

get_domain_for_subtraction:
        sll     $t0, $a0, 1
        li      $t2, 1          # t2=base
        sll     $t2, $t2, $t0
        ori     $t2, $t2, 1
        move    $t3, $zero      # t3=mask
gdfsFor:
        beq     $a2, $zero, gdfsForDone
        andi    $t0, $a2, 1
        beq     $t0, $zero, gdfsSkpi
        srl     $t0, $t2, $a0
        or      $t3, $t3, $t0
gdfsSkpi:
        sll     $t2, $t2, 1
        srl     $a2, $a2, 1
        j       gdfsFor
gdfsForDone:
        and     $v0, $a1, $t3
        jr      $ra

is_single_value_domain:
        beq     $a0, $zero, isvdRet0
        sub     $t0, $a0, 1
        and     $t0, $a0, $t0
        bne     $t0, $zero, isvdRet0
        li      $v0, 1
        jr      $ra
isvdRet0:
        move    $v0, $zero
        jr      $ra

.kdata                          # interrupt handler data (separated just for readability)
chunkIH:        .space 8        # space for two registers

.ktext 0x80000180
interrupt_handler:
.set noat
        move    $k1, $at                # Save $at                               
.set at
        la      $k0, chunkIH
        sw      $a0, 0($k0)             # Get some free registers                  
        sw      $a1, 4($k0)             # by storing them to a global variable     

        mfc0    $k0, $13                # Get Cause register                       
        srl     $a0, $k0, 2                
        and     $a0, $a0, 0xf           # ExcCode field                            
        bne     $a0, 0, done         

interrupt_dispatch:                     # Interrupt:                             
        mfc0    $k0, $13                # Get Cause register, again                 
        beq     $k0, 0, done            # handled all outstanding interrupts     
        
        and     $a0, $k0, BONK_MASK     # is there a bonk interrupt?                
        bne     $a0, 0, bonk_interrupt   
        and     $a0, $k0, TIMER_MASK    # is there a timer interrupt?
        bne     $a0, 0, timer_interrupt
        and     $a0, $k0, ON_FIRE_MASK  # is there a fire interrupt?
        bne     $a0, 0, fire_interrupt
        and     $a0, $k0, MAX_GROWTH_INT_MASK  # is there a max growth interrupt?
        bne     $a0, 0, max_growth_interrupt
        and     $a0, $k0, REQUEST_PUZZLE_INT_MASK  # is there a request puzzle interrupt?
        bne     $a0, 0, request_puzzle_interrupt
        
        j       done

bonk_interrupt:
        sw      $a1, BONK_ACK           # acknowledge interrupt
        sw      $zero, VELOCITY         # ???
        j       interrupt_dispatch      # see if other interrupts are waiting

timer_interrupt:
        sw      $a1, TIMER_ACK          # acknowledge interrupt
        j       interrupt_dispatch      # see if other interrupts are waiting

fire_interrupt:
        sw      $a1, ON_FIRE_ACK        # acknowledge interrupt
        j       interrupt_dispatch      # see if other interrupts are waiting

max_growth_interrupt:
        sw      $a1, MAX_GROWTH_ACK     # acknowledge interrupt
        j       interrupt_dispatch      # see if other interrupts are waiting

request_puzzle_interrupt:
        sw      $a1, REQUEST_PUZZLE_ACK # acknowledge interrupt
	lw      $a1, lock
	add     $a1, $a1, 4096
	and     $a1, $a1, 0x1001FFFF
	or      $a1, $a1, 0x10010000
	sw      $a1, lock
        j       interrupt_dispatch      # see if other interrupts are waiting

done:
        la      $k0, chunkIH
        lw      $a0, 0($k0)             # Restore saved registers
        lw      $a1, 4($k0)
.set noat
        move    $at, $k1                # Restore $at
.set at 
        eret

